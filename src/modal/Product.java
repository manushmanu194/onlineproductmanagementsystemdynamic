package modal;

public class Product {
 
  private String pname;
  private String ptype;
  private Integer pprice;
  private Integer  id;
  public Product() {
	// TODO Auto-generated constructor stub
}
public Product(String pname, String ptype, Integer pprice, Integer id) {
	super();
	this.pname = pname;
	this.ptype = ptype;
	this.pprice = pprice;
	this.id = id;
}
public String getPname() {
	return pname;
}
public void setPname(String pname) {
	this.pname = pname;
}
public String getPtype() {
	return ptype;
}
public void setPtype(String ptype) {
	this.ptype = ptype;
}
public Integer getPprice() {
	return pprice;
}
public void setPprice(Integer pprice) {
	this.pprice = pprice;
}
public Integer getId() {
	return id;
}
public void setId(Integer id) {
	this.id = id;
}
  
 
   


}
