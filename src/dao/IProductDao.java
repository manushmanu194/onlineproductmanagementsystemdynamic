package dao;

import java.util.List;

import modal.Admin;
import modal.Product;



public interface IProductDao {
  public int adminAuthentication(Admin admin);

  public List<Product> viewAll();

  public int add(Product info);

  public void update(Product edit);

  public void delete(Product info);


}
