package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import modal.Admin;
import modal.Product;
import util.DbUtil;
import util.QueryUtil;

public class IProductDaoImp implements IProductDao {
  int result;
  PreparedStatement pst;
  ResultSet rs;
 
  @Override
  public int adminAuthentication(Admin admin) {
    result = 0;
    try {
      pst = DbUtil.getCon().prepareStatement(QueryUtil.adminLogin);
      pst.setString(1, admin.getUname());
      pst.setString(2, admin.getPassword());
      rs = pst.executeQuery();
      while (rs.next()) {
        result++;
        
      }

    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception oocurs in Admin Authentication");
      //e.printStackTrace();
    } 
    return result;
  }

  @Override
  public List<Product> viewAll() {
    List<Product> list = new ArrayList<Product>();
    try {
      pst = DbUtil.getCon().prepareStatement(QueryUtil.viewAll);
      rs = pst.executeQuery();
      while (rs.next()) {
 Product product = new Product(rs.getString(1),rs.getString(2),rs.getInt(3),rs.getInt(4));

       
        list.add(product);
        
      }
    } catch (ClassNotFoundException | SQLException e) {
      System.out.println("Exception occurs in view All Product");
    }
    return list;
  }

  @Override
  public int add(Product info) {
   try {
    pst = DbUtil.getCon().prepareStatement(QueryUtil.add);
    pst.setString(1, info.getPname());
    pst.setString(2, info.getPtype());
    pst.setInt(3, info.getPprice());
    pst.setInt(4, info.getId());

    result = pst.executeUpdate();
  
  } catch (ClassNotFoundException | SQLException e) {
   
  }
   
    return result;
  }



  @Override
  public void delete(Product info) {
    try {
      pst = DbUtil.getCon().prepareStatement(QueryUtil.delete);
      pst.setInt(1, info.getId());
      pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      
    }
  }

  @Override
  public void update(Product edit) {
    try {
      pst = DbUtil.getCon().prepareStatement(QueryUtil.update);
      pst.setString(1, edit.getPname());
      pst.setString(2, edit.getPtype());
      pst.setInt(3, edit.getPprice());
      pst.setInt(4, edit.getId());
      pst.executeUpdate();
    } catch (ClassNotFoundException | SQLException e) {
      
    }
    
  }

}
