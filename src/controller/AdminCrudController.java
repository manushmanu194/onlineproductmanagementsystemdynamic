package controller;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IProductDao;
import dao.IProductDaoImp;
import modal.Product;

@WebServlet({ "/admincrud", "/home", "/delete", "/insert", "/signout",
"/update" })
public class AdminCrudController extends HttpServlet {

	 private static final long serialVersionUID = 1L;

	  protected void doGet(HttpServletRequest request, HttpServletResponse response)
	      throws ServletException, IOException {
	    String url = request.getServletPath();
	    IProductDao dao = new IProductDaoImp();
	    Product product = new Product();

	    if (url.equals("/admincrud")) {

	      List<Product> list = dao.viewAll();

	      request.setAttribute("product", list);
	      request.getRequestDispatcher("admincrud.jsp").forward(request, response);
	    } else if (url.equals("/home")) {
	      List<Product> list = dao.viewAll();
	      request.setAttribute("product", list);
	      request.getRequestDispatcher("admin.jsp").forward(request, response);
	    } else if (url.equals("/delete")) {
	      int id = Integer.parseInt(request.getParameter("id"));
	      product.setId(id);
	      dao.delete(product);
	      PrintWriter out = response.getWriter();
	      response.setContentType("text/html");
	      out.print(id + " deleted successfully<br/>");
	      List<Product> list = dao.viewAll();
	      request.setAttribute("product", list);
	      request.getRequestDispatcher("admincrud.jsp").include(request, response);
	    } else if (url.equals("/insert")) {
	      request.getRequestDispatcher("insert.jsp").forward(request, response);
	    } else if (url.equals("/signout")) {
	      HttpSession session = request.getSession(false);
	      String uname =  (String) session.getAttribute("uname");
	      System.out.println(uname + " logged out @ " + new Date());
	      session.invalidate();
	      request.setAttribute("uname", uname + "  Logged out successfully!!!");
	      request.getRequestDispatcher("logout.jsp").forward(request, response);
	    } else if (url.equals("/update")) {
	      String pname = request.getParameter("pname");
	      String ptype = request.getParameter("ptype");
	      Integer pprice = Integer.parseInt(request.getParameter("pprice"));
	      Integer id = Integer.parseInt(request.getParameter("id"));
	      Product product1 = new Product(pname, ptype, pprice,
	          id);
	      request.setAttribute("product", product1);
	      request.getRequestDispatcher("update.jsp").forward(request, response);
	    }

	  }


}
