package controller;

import java.io.IOException;
import java.util.Date;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dao.IProductDao;
import dao.IProductDaoImp;
import modal.Admin;
import modal.Product;

@WebServlet(name = "AdminController", urlPatterns = { "/login", "/add", "/updateCont" })
public class AdminController extends HttpServlet {

	private static final long serialVersionUID = 1L;

	protected void doPost(HttpServletRequest request,
	      HttpServletResponse response) throws ServletException, IOException {
	    String url = request.getServletPath();
	    IProductDao dao = new IProductDaoImp();

	    if (url.equals("/login")) {
	      String uname = request.getParameter("uname");
	      String password = request.getParameter("password");
	     
	      HttpSession session = request.getSession(true);
	      session.setAttribute("uname", uname);
	      System.out
	          .println(uname + "Logged in @" + new Date(session.getCreationTime()));

	      Admin admin = new Admin(uname, password);
	      int result = dao.adminAuthentication(admin);
	      if (result > 0) {
	        List<Product> list = dao.viewAll();
	        request.setAttribute("product", list);
	        request.getRequestDispatcher("admin.jsp").forward(request, response);
	      } else {
	        request.setAttribute("error",
	            "Hi" + uname + ", Please check your credentials");
	        request.getRequestDispatcher("index.jsp").forward(request, response);
	      }
	    } else if (url.equals("/add")) {
	      String pname = request.getParameter("pname");
	      String ptype = request.getParameter("ptype");
	       Integer pprice = Integer.parseInt(request.getParameter("pprice"));
	      Integer id = Integer.parseInt(request.getParameter("id"));
	      
	      
	      Product product = new Product(pname, ptype, pprice, id);
	      int result = dao.add(product);
	      if (result > 0) {
	        request.setAttribute("msg", id + "inserted succesfully");
	      } else {
	        request.setAttribute("msg", id + "duplicate entry");
	      }
	      List<Product> list = dao.viewAll();
	      request.setAttribute("product", list);
	      request.getRequestDispatcher("admincrud.jsp").forward(request, response);
	    } else if (url.equals("/updateCont")) {
	    	String pname = request.getParameter("pname");
	    
	    	String ptype = request.getParameter("ptype");
	    	  Integer pprice = Integer.parseInt(request.getParameter("pprice"));
	          Integer id = Integer.parseInt(request.getParameter("id"));
	    			       
	     
	     Product product=new Product(pname,ptype,pprice,id);
	      dao.update(product);
	      List<Product> list = dao.viewAll();
	      request.setAttribute("product", list);
	      request.getRequestDispatcher("admincrud.jsp").include(request, response);

	    }
	  }

}
