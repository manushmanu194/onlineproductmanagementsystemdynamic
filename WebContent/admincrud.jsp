<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin CRUD</title>
<style type="text/css">
body{
background-color:#9a8194;}
</style>
</head>
<body>
	<h1>Admin CRUD Operations Page</h1>
	<p align="right">
		Hi,
		<%=(String) session.getAttribute("uname")%>
		<a href="home">Home</a> <a href="insert">Add New Product</a> <a
			href="signout">Logout</a>
	</p>
	<hr />
	${msg}
	<table border="5">
		<tr>
			<th colspan="6">Product Details</th>
		</tr>
		<tr>
			<th>Product Name</th>
			<th>Product Type</th>
			<th>Product Price</th>
			<th>Product Id</th>
			<th>Update</th>
			<th>Delete</th>
		</tr>
		<c:forEach items="${product}" var="f">
			<tr style="text-align: center;">
				<td>${f.getPname()}</td>
				<td>${f.getPtype()}</td>
				<td>${f.getPprice()}</td>
				<td>${f.getId()}</td>
				<td><a style="text-decoration: none"
					href="update?pname=${f.getPname()}&ptype=${f.getPtype()}&pprice=${f.getPprice()}&id=${f.getId()}">Update</a></td>
				<td><a style="text-decoration: none"
					href="delete?id=${f.getId()}">Delete</a>
				</td>
			</tr>

		</c:forEach>
	</table>
</body>
</html>



