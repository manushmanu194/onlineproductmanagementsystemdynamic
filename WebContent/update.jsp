<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Update</title>
<style type="text/css">
body {background-color : #763857;
	
}
</style>
</head>
<body>
	<h1>Admin Update Page</h1>
	Hi,
	<%=(String) session.getAttribute("uname")%><hr />
	<form action="updateCont" method="post">
		<table>
			<tr>
			<td>Product Name :</td>
				<td><input type="text" name="pname"
					value="${product.getPname()}"></td>
				
			</tr>
			<tr>
				<td>Product Type :</td>
				<td><input type="text" name="ptype"
					value="${product.getPtype()}"></td>
			</tr>
			<tr>
				<td>Product Price :</td>
				<td><input type="text" name="pprice"
					value="${product.getPprice()}"></td>
			</tr>
			<tr>
				<td>Product Id:</td>
				<td><input type="text" name="id" readonly="readonly"
					value="${product.getId()}" /></td>
			</tr>
			<tr>
			
				<td><input type="submit" value="Update" /></td>
			</tr>
		</table>


	</form>
</body>
</html>