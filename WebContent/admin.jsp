<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="ISO-8859-1">
<title>Admin</title>
<style type="text/css">
body{
background-color:#c6a9a3;
}
</style>
</head>
<body>
	<h1>Admin Home Page</h1>
	<p align="right">
		Hi,
		<%=(String) session.getAttribute("uname")%>
		<a href="admincrud">Admin CRUD</a> <a
			href="signout">Logout</a>
	</p>
	<hr />

	<table border="5">
		<tr>
			<th colspan="4">Product Details</th>
		</tr>
		<tr>
			<th>Product Name</th>
			<th>Product Type</th>
			<th>Product Price</th>
			<th>Product Id</th>
			
		</tr>
		<c:forEach items="${product}" var="f">
			<tr style="text-align: center;">
				<td>${f.getPname()}</td>
				<td>${f.getPtype()}</td>
				<td>${f.getPprice()}</td>
				<td>${f.getId()}</td>
				

		 	</tr>

		</c:forEach>
	</table>
</body>
</html>